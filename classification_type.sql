CREATE TABLE classification_type
(
    id SERIAL PRIMARY KEY
    name text
)