# coding: utf8
import database

class ClassificationType:
    def __init__(self, name, id=None):
        self.name = name
        self.id = id

    def __repr__(self):
        return "<ClassificationType {}".format(self.name)

    def save_to_db(self):
        with database.CursorFromConnectionFromPool() as cursor:
            # Run some code
            cursor.execute('INSERT INTO classification_type (name) VALUES (%s, %s, %s)', (self.name,))

    @classmethod
    def load_from_db_by_name(cls, name):
        with database.CursorFromConnectionFromPool() as cursor:
            cursor.execute('SELECT * FROM classification_type WHERE name=%s', (name,))
            data = cursor.fetchone()
            return cls(data[1], data[2])

class ClassificationEntry:
    def __init__(self, code, name, description, type, parent_id, id=None):
        self.name = name
        self.id = id

    def __repr__(self):
        return "<ClassificationType {}".format(self.name)