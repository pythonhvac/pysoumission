CREATE TABLE classification_entry
(
    id SERIAL PRIMARY KEY
    code text
    name text
    type integer
    parent_id integer
    description text
)