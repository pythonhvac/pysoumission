# coding: utf8
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from database import Database, CursorFromConnectionFromPool
from classification import ClassificationType, ClassificationEntry

class LoginWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Database Connection")

        grid = Gtk.Grid()
        self.add(grid)

        self.user = Gtk.Entry()
        self.user.set_text("postgres")
        self.password = Gtk.Entry()
        self.password.set_visibility(False)
        self.host = Gtk.Entry()
        self.host.set_text("localhost")
        self.database = Gtk.Entry()
        self.database.set_text("postgres")
        connect_button = Gtk.Button.new_with_mnemonic("_Connect")
        connect_button.connect("clicked", self.on_connect_clicked)

        grid.add(self.user)
        grid.attach_next_to(self.password, self.user, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.host, self.user, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.database, self.host, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(connect_button, self.password, Gtk.PositionType.BOTTOM, 2, 1)

    def on_connect_clicked(self, button):
        try:
            Database.initialise(user=self.user.get_text(),
                                password=self.password.get_text(),
                                database=self.database.get_text(),
                                host=self.host.get_text()
                                )
            Gtk.main_quit()
        except:
            print("fail")
            raise


window = LoginWindow()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()





new_type = ClassificationType("CFC")
